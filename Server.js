var express = require("express");
var app = express();
var mongoose = require("mongoose");
mongoose.connect("mongodb://admin:admin@ds125060.mlab.com:25060/provamongo", function(err) {
  if(err) {
    console.log(err);
  } else {
    console.log("connesso");
  }
});

var Schema = mongoose.Schema;
var utenteSchema = new Schema({
  nome: String,
  cognome: String,
  eta: Number,
  amici: [
    {
      nome: String,
      cognome: String,
    }
  ],
});
var User = mongoose.model("User", utenteSchema);

var Stefano = new User({
  nome: "Stefano",
  cognome: "Nova",
  eta: 25,
  amici: [
    {
      nome: "Daniele",
      cognome: "Fuscaldo",
    }
  ]
});

var Daniele = new User({
  nome: "Daniele",
  cognome: "Fuscaldo",
  eta: 24,
  amici: [
    {
      nome: "Stefano",
      cognome: "Nova",
    }
  ]
});

//Salvataggio
Stefano.save().then(function() {
  console.log("Stefano salvato nel DB");
}).catch(function(err) {
  console.log(err);
})

//Ricerca
User.find().exec().then(function(data) {
  console.log("tutti gli utenti: " + data);
}).catch();

User.find({"nome": "Daniele"}).exec().then(function(data) {
  console.log("utenti di nome Daniele: " + data);
}).catch();

User.find({"eta": {$lt:18}}).exec().then(function(data) {
  console.log("utenti minorenni: " + data);
}).catch();

//Update
User.findOne({"nome": "Stefano"}).exec().then(function(data) {
  data.eta -= 10;
  return data.save();
}).then(function() {
  console.log('utente aggiornato');
}).catch(function(err) {
  throw err;
});

User.findOneAndUpdate("Stefano", {nome:"Simone"}).exec().then(function(data) {
  console.log('utente aggiornato');
}).catch(function (err) {
  throw err;
});

//Delete
User.findByIdAndRemove("58c13b50ff3a40081ca79b98").exec().then(function() {
  console.log('utente rimosso dal DB');
}).catch(function(err) {
  throw err;
});

app.get("/", function(req, res) {
  res.send("miao");
});
app.listen(3000, function() {
  console.log("http://localhost:3000");
});
